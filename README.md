# Ult Calculator

    This calculator accepts dynamic input - scientific style input.

    For example:  3*4-6/7--7.45

    It does not support scientific functions like sqrt(), pow(), log()

## How to compile

System with make
	run make in the terminal
```	
    make
```
Windows systems without make run	 compile.bat

currently automated builds in windows are only supported by g++
Plans are to include mingwin32 and vc++